#ifndef COMMON_H
#define COMMON_H

#include <stdbool.h>
#include <cairo.h>
#include <wayland-client-protocol.h>

#include "viewporter-protocol-client.h"
#include "xdg-shell-protocol-client.h"
#include "fractional-scale-v1-protocol-client.h"

#define BUFFER_POOL_SIZE 4096

struct buffer {
        struct wl_buffer *wl_buffer;
        void *data;
        cairo_surface_t *surface;
        cairo_t *cairo;
	int32_t width, height;
        int32_t size;
        bool busy;
};

struct surface {
	struct ctx *ctx;
	struct wl_surface *wl_surface;
	struct wp_viewport *wp_viewport;
	struct wp_fractional_scale_v1 *scale;
	double server_factor;
};

struct toplevel {
	struct surface base;

	struct xdg_surface *xdg_surface;
	struct xdg_toplevel *xdg_toplevel;

	double req_width, req_height; // In logical space
	bool resized;
};

struct subsurface {
	struct surface base;

	struct wl_subsurface *wl_subsurface;
};

struct ctx {
	struct wl_display *wl_display;
	struct wl_shm *wl_shm;
	struct wl_compositor *wl_compositor;
	struct wl_subcompositor *wl_subcompositor;
	struct xdg_wm_base *xdg_wm_base;
	struct wp_viewporter *wp_viewporter;
	struct wl_seat *wl_seat;
	struct wl_pointer *wl_pointer;
	
	struct wp_fractional_scale_manager_v1 *wp_fractional_scale_manager_v1;

	struct surface *active;
	double active_x, active_y; // In logical space
	
	bool running;

	void (*draw)(struct surface *surface);

	struct buffer buffer_pool[BUFFER_POOL_SIZE];
};

void ctx_init(struct ctx *ctx);

void toplevel_init(struct ctx *ctx, struct toplevel *toplevel);

void subsurface_init(struct ctx *ctx, struct subsurface *subsurface,
	struct surface *parent);

struct buffer *get_buffer(struct ctx *ctx, int32_t width, int32_t height);
 
#endif
