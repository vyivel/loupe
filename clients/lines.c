#include "common.h"

static void draw(struct surface *surface) {
	struct toplevel *toplevel = (struct toplevel *)surface;

	double toplevel_factor = surface->server_factor;
	wp_fractional_scale_v1_set_scale_factor(surface->scale,
		(uint32_t)(toplevel_factor * (1 << 24)));

	// In pixels!
	int width = toplevel->req_width * toplevel_factor;
	int height = toplevel->req_height * toplevel_factor;

	struct buffer *buffer = get_buffer(surface->ctx, width, height);
	if (buffer == NULL) {
		return;
	}

	cairo_t *cairo = buffer->cairo;

	cairo_set_source_rgba(cairo, 0.0, 0.0, 0.0, 1.0);
	cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
	cairo_paint(cairo);

	cairo_set_line_width(cairo, 1);
	cairo_set_source_rgba(cairo, 1.0, 1.0, 1.0, 1.0);
	for (int y = 0; y < height; y += 2) {
		cairo_move_to(cairo, 0, y + 0.5);
		cairo_line_to(cairo, width / 2.0, y + 0.5);
		cairo_stroke(cairo);
	}
	for (int x = width / 2; x < width; x += 2) {
		cairo_move_to(cairo, x + 0.5, 0);
		cairo_line_to(cairo, x + 0.5, height);
		cairo_stroke(cairo);
	}

	wl_surface_damage_buffer(surface->wl_surface, 0, 0, width, height);
	wl_surface_attach(surface->wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(surface->wl_surface);
}

int main() {
	struct ctx ctx = {0};
	ctx_init(&ctx);
	struct toplevel toplevel = {0};
	toplevel_init(&ctx, &toplevel);
	toplevel.req_width = 300;
	toplevel.req_height = 200;

	ctx.draw = draw;

	while (ctx.running && wl_display_dispatch(ctx.wl_display) != -1) {
		/* Wait */
	}

	return 0;
}
