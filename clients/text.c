#include <pango/pangocairo.h>

#include "common.h"

static PangoFontDescription *font_desc = NULL;

static void draw(struct surface *surface) {
	struct toplevel *toplevel = (struct toplevel *)surface;

	double toplevel_factor = surface->server_factor;
	wp_fractional_scale_v1_set_scale_factor(surface->scale,
		(uint32_t)(toplevel_factor * (1 << 24)));

	// In pixels!
	int width = toplevel->req_width * toplevel_factor;
	int height = toplevel->req_height * toplevel_factor;

	struct buffer *buffer = get_buffer(surface->ctx, width, height);
	if (buffer == NULL) {
		return;
	}

	cairo_t *cairo = buffer->cairo;

	static const char buf[] =
		"A scale of one equals a lack of scaling, where the communicated values\n"
		"define both logical coordinates and pixels.\n"
		"A scale greater than one describes that for every logical coordinate,\n"
		"more than one pixel is used, and a scale less than one describes that\n"
		"multiple logical coordinates make up one pixel.\n"
		"In mathematical terms, logical coordinates can be obtained by dividing\n"
		"the provided values by the currently active scale.";

	cairo_set_source_rgba(cairo, 1.0, 1.0, 1.0, 1.0);
	cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
	cairo_paint(cairo);

	PangoLayout *layout = pango_cairo_create_layout(cairo);
	pango_layout_set_text(layout, buf, -1);
	pango_layout_set_font_description(layout, font_desc);
	pango_layout_set_width(layout, width * PANGO_SCALE);
	pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);
	pango_layout_set_ellipsize(layout, PANGO_ELLIPSIZE_END);

	PangoAttrList *attrs = pango_attr_list_new();
	pango_attr_list_insert(attrs, pango_attr_scale_new(toplevel_factor));
	pango_layout_set_attributes(layout, attrs);
	pango_attr_list_unref(attrs);

	int label_width, label_height;
	pango_layout_get_pixel_size(layout, &label_width, &label_height);

	cairo_move_to(cairo, 0, (height - label_height) / 2);
	cairo_set_source_rgba(cairo, 0.0, 0.0, 0.0, 1.0);
	pango_cairo_show_layout(cairo, layout);

	g_object_unref(layout);

	wl_surface_damage_buffer(surface->wl_surface, 0, 0, width, height);
	wl_surface_attach(surface->wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(surface->wl_surface);
}

int main() {
	font_desc = pango_font_description_from_string("Liberation Sans 14");

	struct ctx ctx = {0};
	ctx_init(&ctx);
	struct toplevel toplevel = {0};
	toplevel_init(&ctx, &toplevel);
	toplevel.req_width = 600;
	toplevel.req_height = 400;

	ctx.draw = draw;

	while (ctx.running && wl_display_dispatch(ctx.wl_display) != -1) {
		/* Wait */
	}

	return 0;
}
