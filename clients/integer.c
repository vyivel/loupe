#include <math.h>

#include "common.h"

static void draw(struct surface *surface) {
	struct toplevel *toplevel = (struct toplevel *)surface;

	double toplevel_factor = ceil(surface->server_factor);
	wp_fractional_scale_v1_set_scale_factor(surface->scale,
		(uint32_t)(toplevel_factor * (1 << 24)));

	// In pixels!
	int width = toplevel->req_width * toplevel_factor;
	int height = toplevel->req_height * toplevel_factor;

	struct buffer *buffer = get_buffer(surface->ctx, width, height);
	if (buffer == NULL) {
		return;
	}

	cairo_t *cairo = buffer->cairo;

	cairo_set_source_rgba(cairo, 1.0, 1.0, 0.0, 1.0);
	cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
	cairo_paint(cairo);

	if (surface->ctx->active != 0) {
		double x = surface->ctx->active_x * toplevel_factor;
		double y = surface->ctx->active_y * toplevel_factor;
		double r = 10 * toplevel_factor;
		cairo_set_source_rgba(cairo, 0.0, 0.0, 0.0, 1.0);
		cairo_rectangle(cairo,
			x - r, y - r, r * 2, r * 2);
		cairo_set_line_width(cairo, 3);
		cairo_stroke(cairo);
	}

	wl_surface_damage_buffer(surface->wl_surface, 0, 0, width, height);
	wl_surface_attach(surface->wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(surface->wl_surface);
}

int main() {
	struct ctx ctx = {0};
	ctx_init(&ctx);
	struct toplevel toplevel = {0};
	toplevel_init(&ctx, &toplevel);
	toplevel.req_width = 600;
	toplevel.req_height = 400;

	ctx.draw = draw;

	while (ctx.running && wl_display_dispatch(ctx.wl_display) != -1) {
		/* Wait */
	}

	return 0;
}
