#include <math.h>
#include <stdio.h>

#include "common.h"

static int nframes = 0;

static const struct wl_callback_listener callback_listener;

static void draw(struct surface *surface);

static void callback_handle_done(void *data, struct wl_callback *callback,
		uint32_t time_ms) {
	draw(data);
}

static const struct wl_callback_listener callback_listener = {
	.done = callback_handle_done,
};

static void draw(struct surface *surface) {
	struct toplevel *toplevel = (struct toplevel *)surface;

	// 1..2
	double toplevel_factor = (sin(nframes / 360.0) + 3) / 2;
	printf("[+] Current factor: %lf (%d frames)\n", toplevel_factor, nframes);

	wp_fractional_scale_v1_set_scale_factor(surface->scale,
		(uint32_t)(toplevel_factor * (1 << 24)));

	// In pixels!
	int width = toplevel->req_width * toplevel_factor;
	int height = toplevel->req_height * toplevel_factor;

	struct buffer *buffer = get_buffer(surface->ctx, width, height);
	if (buffer == NULL) {
		return;
	}

	cairo_t *cairo = buffer->cairo;

	cairo_set_source_rgba(cairo, 1.0, 1.0, 1.0, 1.0);
	cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
	cairo_paint(cairo);

	cairo_set_source_rgba(cairo, 1.0, 0.0, 1.0, 1.0);
	cairo_set_line_width(cairo, 2 * toplevel_factor);
	cairo_rectangle(cairo,
		width / 3.0, height / 3.0,
		width / 3.0, height / 3.0);
	cairo_stroke(cairo);

	wl_surface_damage_buffer(surface->wl_surface, 0, 0, width, height);
	wl_surface_attach(surface->wl_surface, buffer->wl_buffer, 0, 0);

	struct wl_callback *callback = wl_surface_frame(surface->wl_surface);
	wl_callback_add_listener(callback, &callback_listener, &toplevel->base);

	wl_surface_commit(surface->wl_surface);
	++nframes;
}

int main() {
	struct ctx ctx = {0};
	ctx_init(&ctx);
	struct toplevel toplevel = {0};
	toplevel_init(&ctx, &toplevel);
	toplevel.req_width = 600;
	toplevel.req_height = 400;

	ctx.draw = draw;

	while (ctx.running && wl_display_dispatch(ctx.wl_display) != -1) {
		/* Wait */
	}

	return 0;
}
