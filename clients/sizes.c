#include <pango/pangocairo.h>

#include "common.h"

static PangoFontDescription *font_desc = NULL;

static struct subsurface red = {0};
static struct subsurface green = {0};
static struct subsurface blue = {0};
static struct subsurface yellow = {0};
static struct subsurface magenta = {0};
static struct subsurface cyan = {0};

static struct toplevel toplevel = {0};

static const int W = 240;

static void send_factor(struct surface *surface, double factor) {
	wp_fractional_scale_v1_set_scale_factor(surface->scale,
		(uint32_t)(factor * (1 << 24)));
}

static void draw(struct surface *surface) {
	int width, height;
	struct buffer *buffer = NULL;
	cairo_t *cairo = NULL;

	double buffer_factor = 1.0;
	double commit_factor = 15.0; // Irrelevant
	int buffer_scale = 1;

	// surface-local
	double offx = 0.0, offy = 0.0;

	if (surface == &toplevel.base) {
		send_factor(&toplevel.base, 1.0);

		width = toplevel.req_width * buffer_factor * buffer_scale;
		height = toplevel.req_height * buffer_factor * buffer_scale;
		buffer = get_buffer(surface->ctx, width, height);
		if (buffer == NULL) {
			return;
		}
		cairo = buffer->cairo;

		cairo_set_source_rgba(cairo, 1.0, 1.0, 1.0, 1.0);
		cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
		cairo_paint(cairo);
	} else if (surface == &red.base) {
		buffer_factor = 1.5;

		width = W * buffer_factor * buffer_scale;
		height = W * buffer_factor * buffer_scale;

		buffer = get_buffer(surface->ctx, width, height);
		if (buffer == NULL) {
			return;
		}
		cairo = buffer->cairo;

		cairo_set_source_rgba(cairo, 1.0, 0.0, 0.0, 1.0);
		cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
		cairo_paint(cairo);
	} else if (surface == &green.base) {
		buffer_factor = 1.5;
		commit_factor = 3.0;
		buffer_scale = 2;

		width = W * buffer_factor * buffer_scale;
		height = W * buffer_factor * buffer_scale;

		buffer = get_buffer(surface->ctx, width, height);
		if (buffer == NULL) {
			return;
		}
		cairo = buffer->cairo;

		cairo_set_source_rgba(cairo, 0.0, 1.0, 0.0, 1.0);
		cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
		cairo_paint(cairo);
	} else if (surface == &blue.base) {
		width = W * buffer_factor * buffer_scale * 3;
		height = W * buffer_factor * buffer_scale * 3;

		buffer = get_buffer(surface->ctx, width, height);
		if (buffer == NULL) {
			return;
		}
		cairo = buffer->cairo;

		cairo_set_source_rgba(cairo, 0.0, 0.0, 1.0, 1.0);
		cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
		cairo_paint(cairo);

		send_factor(surface, buffer_factor);
		wp_viewport_set_source(surface->wp_viewport,
			wl_fixed_from_double(W * buffer_factor),
			wl_fixed_from_double(W * buffer_factor),
			wl_fixed_from_double(W * buffer_factor),
			wl_fixed_from_double(W * buffer_factor));
		offx = W;
		offy = W;
	} else if (surface == &yellow.base) {
		buffer_factor = 0.45;
		buffer_scale = 2;

		width = W * buffer_factor * buffer_scale * 3;
		height = W * buffer_factor * buffer_scale * 3;

		buffer = get_buffer(surface->ctx, width, height);
		if (buffer == NULL) {
			return;
		}
		cairo = buffer->cairo;

		cairo_set_source_rgba(cairo, 1.0, 1.0, 0.0, 1.0);
		cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
		cairo_paint(cairo);

		send_factor(surface, buffer_factor);
		wp_viewport_set_source(surface->wp_viewport,
			wl_fixed_from_double(W * buffer_factor),
			wl_fixed_from_double(W * buffer_factor),
			wl_fixed_from_double(W * buffer_factor),
			wl_fixed_from_double(W * buffer_factor));
		offx = W;
		offy = W;
	} else if (surface == &magenta.base) {
		double dst_factor = 15.0; // Irrelevant

		buffer_factor = 1.15;

		width = W * buffer_factor * buffer_scale;
		height = W * buffer_factor * buffer_scale;

		buffer = get_buffer(surface->ctx, width, height);
		if (buffer == NULL) {
			return;
		}
		cairo = buffer->cairo;

		cairo_set_source_rgba(cairo, 1.0, 0.0, 1.0, 1.0);
		cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
		cairo_paint(cairo);

		send_factor(surface, dst_factor);
		wp_viewport_set_destination(surface->wp_viewport,
			W * dst_factor, W * dst_factor);
	} else if (surface == &cyan.base) {
		width = W * buffer_factor * buffer_scale * 3;
		height = W * buffer_factor * buffer_scale * 3;

		buffer = get_buffer(surface->ctx, width, height);
		if (buffer == NULL) {
			return;
		}
		cairo = buffer->cairo;

		cairo_set_source_rgba(cairo, 0.0, 1.0, 1.0, 1.0);
		cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
		cairo_paint(cairo);

		send_factor(surface, buffer_factor);
		wp_viewport_set_source(surface->wp_viewport,
			wl_fixed_from_double(W * buffer_factor),
			wl_fixed_from_double(W * buffer_factor),
			wl_fixed_from_double(W * buffer_factor),
			wl_fixed_from_double(W * buffer_factor));
		offx = W;
		offy = W;
	}

	char buf[128];
	double factor = buffer_factor * buffer_scale;
	snprintf(buf, sizeof(buf),
		"buf %dx%d\nsrc %.1lfx%.1lf\nbf %lf\nf %lf\noff %.1lf,%.1lf",
		width, height, W * factor, W * factor, buffer_factor, factor, offx, offy);

	PangoLayout *layout = pango_cairo_create_layout(cairo);
	pango_layout_set_text(layout, buf, -1);
	pango_layout_set_font_description(layout, font_desc);
	pango_layout_set_width(layout, W * factor * PANGO_SCALE);
	pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);

	PangoAttrList *attrs = pango_attr_list_new();
	pango_attr_list_insert(attrs, pango_attr_scale_new(factor));
	pango_layout_set_attributes(layout, attrs);
	pango_attr_list_unref(attrs);

	int label_width, label_height;
	pango_layout_get_pixel_size(layout, &label_width, &label_height);

	cairo_move_to(cairo, offx * factor,
		offy * factor + (W * factor - label_height) / 2);
	cairo_set_source_rgba(cairo, 0.0, 0.0, 0.0, 1.0);
	pango_cairo_show_layout(cairo, layout);

	g_object_unref(layout);

	if (surface == surface->ctx->active) {
		double x = (surface->ctx->active_x + offx) * factor;
		double y = (surface->ctx->active_y + offy) * factor;
		double r = 10 * factor;
		cairo_set_source_rgba(cairo, 0.0, 0.0, 0.0, 1.0);
		cairo_rectangle(cairo,
			x - r, y - r, r * 2, r * 2);
		cairo_set_line_width(cairo, 3 * factor);
		cairo_stroke(cairo);
	}

	// wl_surface.damage_buffer is unaffected by scaling
	wl_surface_damage_buffer(surface->wl_surface, 0, 0, width, height);

	send_factor(surface, buffer_factor);
	wl_surface_attach(surface->wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_set_buffer_scale(surface->wl_surface, buffer_scale);

	send_factor(surface, commit_factor); // Shouldn't affect anything
	wl_surface_commit(surface->wl_surface);
}

int main() {
	font_desc = pango_font_description_from_string("monospace 20");

	struct ctx ctx = {0};
	ctx_init(&ctx);
	toplevel_init(&ctx, &toplevel);
	toplevel.req_width = W * 3;
	toplevel.req_height = W * 3;

	subsurface_init(&ctx, &red, &toplevel.base);
	subsurface_init(&ctx, &green, &toplevel.base);
	subsurface_init(&ctx, &blue, &toplevel.base);
	subsurface_init(&ctx, &yellow, &toplevel.base);
	subsurface_init(&ctx, &magenta, &toplevel.base);
	subsurface_init(&ctx, &cyan, &toplevel.base);

	wl_subsurface_set_position(red.wl_subsurface,
		W * 0, W * 0);
	wl_subsurface_set_position(green.wl_subsurface,
		W * 1, W * 0);
	wl_subsurface_set_position(blue.wl_subsurface,
		W * 0, W * 1);
	wl_subsurface_set_position(yellow.wl_subsurface,
		W * 1, W * 1);
	wl_subsurface_set_position(magenta.wl_subsurface,
		W * 0, W * 2);
	wl_subsurface_set_position(cyan.wl_subsurface,
		W * 1, W * 2);

	ctx.draw = draw;

	draw(&red.base);
	draw(&green.base);
	draw(&blue.base);
	draw(&yellow.base);
	draw(&magenta.base);
	draw(&cyan.base);

	while (ctx.running && wl_display_dispatch(ctx.wl_display) != -1) {
		/* Wait */
	}

	return 0;
}
