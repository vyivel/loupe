#include "common.h"

static struct subsurface red = {0}, blue = {0};
static struct toplevel toplevel = {0};

static double toplevel_factor = 1.0;
static double red_factor = 1.0;
static double blue_factor = 1.0;

// Toplevel space
static const int red_x = 70, red_y = 70;
// Red space
static const int red_width = 120, red_height = 120;
// Red space
static const int blue_x = 150, blue_y = 150;
// Blue space
static const int blue_width = 180, blue_height = 90;

static void draw(struct surface *surface) {
	int width, height;
	struct buffer *buffer = NULL;
	double factor = 1.0;
	cairo_t *cairo = NULL;

	if (surface == &toplevel.base) {
		factor = toplevel_factor = surface->server_factor;
		wp_fractional_scale_v1_set_scale_factor(surface->scale,
			(uint32_t)(factor * (1 << 24)));

		wl_subsurface_set_position(red.wl_subsurface,
			red_x * toplevel_factor, red_y * toplevel_factor);

		width = toplevel.req_width * toplevel_factor;
		height = toplevel.req_height * toplevel_factor;
		buffer = get_buffer(surface->ctx, width, height);
		if (buffer == NULL) {
			return;
		}

		cairo = buffer->cairo;

		cairo_set_source_rgba(cairo, 1.0, 1.0, 1.0, 1.0);
		cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
		cairo_paint(cairo);

		cairo_set_source_rgba(cairo, 0.0, 0.0, 0.0, 1.0);
		cairo_rectangle(cairo,
			(int)(red_x * toplevel_factor),
			(int)(red_y * toplevel_factor),
			(int)(red_width * toplevel_factor),
			(int)(red_height * toplevel_factor));
		cairo_fill(cairo);

		cairo_set_source_rgba(cairo, 0.0, 0.0, 0.0, 1.0);
		cairo_rectangle(cairo,
			(int)((red_x + blue_x) * toplevel_factor),
			(int)((red_y + blue_y) * toplevel_factor),
			(int)(blue_width * toplevel_factor),
			(int)(blue_height * toplevel_factor));
		cairo_fill(cairo);
	} else if (surface == &red.base) {
		factor = red_factor = 2.0;
		wp_fractional_scale_v1_set_scale_factor(surface->scale,
			(uint32_t)(factor * (1 << 24)));

		wl_subsurface_set_position(blue.wl_subsurface,
			blue_x * red_factor, blue_y * red_factor);

		width = red_width * red_factor;
		height = red_height * red_factor;
		buffer = get_buffer(surface->ctx, width, height);
		if (buffer == NULL) {
			return;
		}

		cairo = buffer->cairo;
		cairo_set_source_rgba(cairo, 0.5, 0.0, 0.0, 0.5);
		cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
		cairo_paint(cairo);
	} else if (surface == &blue.base) {
		factor = blue_factor = surface->server_factor / 2;
		wp_fractional_scale_v1_set_scale_factor(surface->scale,
			(uint32_t)(factor * (1 << 24)));

		width = blue_width * blue_factor;
		height = blue_height * blue_factor;
		buffer = get_buffer(surface->ctx, width, height);
		if (buffer == NULL) {
			return;
		}

		cairo = buffer->cairo;
		cairo_set_source_rgba(cairo, 0.0, 0.0, 0.5, 0.5);
		cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
		cairo_paint(cairo);
	}

	if (surface == surface->ctx->active) {
		double x = surface->ctx->active_x * factor;
		double y = surface->ctx->active_y * factor;
		double r = 10 * factor;
		cairo_set_source_rgba(cairo, 0.0, 1.0, 0.0, 1.0);
		cairo_rectangle(cairo,
			x - r, y - r, r * 2, r * 2);
		cairo_set_line_width(cairo, 3);
		cairo_stroke(cairo);
	}

	wl_surface_damage_buffer(surface->wl_surface, 0, 0, width, height);
	wl_surface_attach(surface->wl_surface, buffer->wl_buffer, 0, 0);
	wl_surface_commit(surface->wl_surface);
}

int main() {
	struct ctx ctx = {0};
	ctx_init(&ctx);
	toplevel_init(&ctx, &toplevel);
	toplevel.req_width = 640;
	toplevel.req_height = 480;

	subsurface_init(&ctx, &red, &toplevel.base);
	subsurface_init(&ctx, &blue, &red.base);

	ctx.draw = draw;

	draw(&red.base);
	draw(&blue.base);

	while (ctx.running && wl_display_dispatch(ctx.wl_display) != -1) {
		/* Wait */
	}

	return 0;
}
