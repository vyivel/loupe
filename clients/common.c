#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <wordexp.h>

#include "common.h"

static void buffer_handle_release(void *data, struct wl_buffer *wl_buffer) {
	struct buffer *buffer = data;
	buffer->busy = false;
}

static const struct wl_buffer_listener buffer_listener = {
	.release = buffer_handle_release,
};

static bool init_buffer(struct ctx *ctx, struct buffer *buffer,
		int32_t width, int32_t height) {
	bool result = false;

	uint32_t stride = width * 4;
	size_t size = stride * height;

	wordexp_t p;
	static const char *path = "\"$XDG_RUNTIME_DIR/shm-XXXXXX\"";
	if (wordexp(path, &p, WRDE_UNDEF) != 0) {
		fprintf(stderr, "create_pool_file(): XDG_RUNTIME_DIR is not set\n");
		goto end;
	}

	char *name = p.we_wordv[0];
	int fd = mkstemp(name);
	unlink(name);
	wordfree(&p);

	if (fd < 0) {
		goto end;
	}

        int flags = fcntl(fd, F_GETFD);
        if (flags == -1) {
		fprintf(stderr, "create_pool_file(): fnctl(): %s\n",
			strerror(errno));
		goto close_fd;
        }
        if (fcntl(fd, F_SETFD, flags | FD_CLOEXEC) == -1) {
		fprintf(stderr, "create_pool_file(): fnctl(): %s\n",
			strerror(errno));
		goto close_fd;
        }

	if (ftruncate(fd, size) < 0) {
		goto close_fd;
	}

	struct wl_shm_pool *pool = wl_shm_create_pool(ctx->wl_shm, fd, size);
	if (pool == NULL) {
		fprintf(stderr, "create_pool_file(): failed to wl_shm_create_pool()");
		goto close_fd;
	}
	void *data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (data == MAP_FAILED) {
		fprintf(stderr, "create_pool_file(): mmap(): %s\n",
			strerror(errno));
		goto destroy_pool;
	}

	struct wl_buffer *wl_buffer = wl_shm_pool_create_buffer(
		pool, 0, width, height, stride, WL_SHM_FORMAT_ARGB8888);
	if (wl_buffer == NULL) {
		fprintf(stderr, "create_pool_file(): failed to wl_shm_pool_create_buffer()");
		goto munmap_data;
	}
	cairo_surface_t *surface = cairo_image_surface_create_for_data(
		data, CAIRO_FORMAT_ARGB32, width, height, stride);
	if (surface == NULL) {
		fprintf(stderr, "create_pool_file(): failed to cairo_image_surface_create_for_data()");
		goto destroy_buffer;
	}
	cairo_t *cairo = cairo_create(surface);
	if (cairo == NULL) {
		fprintf(stderr, "create_pool_file(): failed to cairo_create()");
		goto destroy_surface;
	}
	
	buffer->wl_buffer = wl_buffer;
	wl_buffer_add_listener(wl_buffer, &buffer_listener, buffer);

	buffer->data = data;
	buffer->surface = surface;
	buffer->cairo = cairo;

	buffer->width = width;
	buffer->height = height;
	buffer->size = size;
	
	result = true;
	goto destroy_pool;

	/* This is only reached on error */
destroy_surface:
	cairo_surface_destroy(surface);
destroy_buffer:
	wl_buffer_destroy(wl_buffer);
munmap_data:
	munmap(data, size);

	/* This is always reached */
destroy_pool:
	wl_shm_pool_destroy(pool);
close_fd:
	close(fd);
end:
	return result;
}

static void finish_buffer(struct buffer *buffer) {
	if (buffer->wl_buffer) {
		munmap(buffer->data, buffer->size);
		cairo_surface_destroy(buffer->surface);
		cairo_destroy(buffer->cairo);

		wl_buffer_destroy(buffer->wl_buffer);
		buffer->wl_buffer = NULL;
	}
}

struct buffer *get_buffer(struct ctx *ctx, int32_t width, int32_t height) {
	for (size_t i = 0; i < BUFFER_POOL_SIZE; i++) {
		struct buffer *buffer = &ctx->buffer_pool[i];
		if (buffer->busy) {
			continue;
		}
		if (buffer->width != width || buffer->height != height) {
			finish_buffer(buffer);
		}
		if (buffer->wl_buffer == NULL &&
				!init_buffer(ctx, buffer, width, height)) {
			return NULL;
		}
		buffer->busy = true;
		return buffer;
	}
	fprintf(stderr, "Failed to get %dx%d buffer\n", width, height);
	return NULL;
}

static void registry_handle_global(void *data, struct wl_registry *registry,
		uint32_t name, const char *interface, uint32_t version) {
	struct ctx *ctx = data;
	if (strcmp(interface, xdg_wm_base_interface.name) == 0) {
		ctx->xdg_wm_base = wl_registry_bind(registry, name,
			&xdg_wm_base_interface, 2);
	} else if (strcmp(interface, wl_shm_interface.name) == 0) {
		ctx->wl_shm = wl_registry_bind(registry, name,
			&wl_shm_interface, 1);
	} else if (strcmp(interface, wl_compositor_interface.name) == 0) {
		ctx->wl_compositor = wl_registry_bind(registry, name,
			&wl_compositor_interface, 4);
	} else if (strcmp(interface, wl_subcompositor_interface.name) == 0) {
		ctx->wl_subcompositor = wl_registry_bind(registry, name,
			&wl_subcompositor_interface, 1);
	} else if (strcmp(interface, wp_viewporter_interface.name) == 0) {
		ctx->wp_viewporter = wl_registry_bind(registry, name,
			&wp_viewporter_interface, 1);
	} else if (strcmp(interface, wl_seat_interface.name) == 0) {
		ctx->wl_seat = wl_registry_bind(registry, name,
			&wl_seat_interface, 1);
	} else if (strcmp(interface, wp_fractional_scale_manager_v1_interface.name) == 0) {
		ctx->wp_fractional_scale_manager_v1 = wl_registry_bind(registry, name,
			&wp_fractional_scale_manager_v1_interface, 1);
	}
}

static void registry_handle_global_remove(void *data,
		struct wl_registry *registry, uint32_t name) {
	// no-op
}

static const struct wl_registry_listener registry_listener = {
	.global = registry_handle_global,
	.global_remove = registry_handle_global_remove,
};

static void xdg_surface_handle_configure(void *data,
		struct xdg_surface *xdg_surface, uint32_t serial) {
	struct toplevel *toplevel = data;
	xdg_surface_ack_configure(xdg_surface, serial);
	toplevel->base.ctx->draw(&toplevel->base);
	toplevel->resized = false;
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

static void xdg_toplevel_handle_configure(void *data,
		struct xdg_toplevel *xdg_toplevel,
		int32_t width, int32_t height, struct wl_array *states) {
	struct toplevel *toplevel = data;
	if (width != 0 && height != 0) {
		toplevel->req_width = width / toplevel->base.server_factor;
		toplevel->req_height = height / toplevel->base.server_factor;
		toplevel->resized = true;
	}
}

static void xdg_toplevel_handle_close(void *data,
		struct xdg_toplevel *xdg_toplevel) {
	struct toplevel *toplevel = data;
	toplevel->base.ctx->running = false;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.configure = xdg_toplevel_handle_configure,
	.close = xdg_toplevel_handle_close,
};

static void pointer_handle_enter(void *data,
		struct wl_pointer *wl_pointer, uint32_t serial,
		struct wl_surface *wl_surface,
		wl_fixed_t surface_x, wl_fixed_t surface_y) {
	struct ctx *ctx = data;
	ctx->active = wl_surface_get_user_data(wl_surface);
	ctx->active_x = wl_fixed_to_double(surface_x) / ctx->active->server_factor;
	ctx->active_y = wl_fixed_to_double(surface_y) / ctx->active->server_factor;
}

static void pointer_handle_leave(void *data,
		struct wl_pointer *wl_pointer, uint32_t serial,
		struct wl_surface *wl_surface) {
	struct ctx *ctx = data;
	struct surface *active = ctx->active;
	ctx->active = NULL;
	ctx->draw(active);
}

static void pointer_handle_motion(void *data,
		struct wl_pointer *wl_pointer, uint32_t time,
		wl_fixed_t surface_x, wl_fixed_t surface_y) {
	struct ctx *ctx = data;
	ctx->active_x = wl_fixed_to_double(surface_x) / ctx->active->server_factor;
	ctx->active_y = wl_fixed_to_double(surface_y) / ctx->active->server_factor;
	ctx->draw(ctx->active);
}

static void pointer_handle_button(void *data,
		struct wl_pointer *wl_pointer, uint32_t serial,
		uint32_t time, uint32_t button, uint32_t state) {
	// TODO
}

static void pointer_handle_axis(void *data,
		struct wl_pointer *wl_pointer, uint32_t time,
		uint32_t axis, wl_fixed_t value) {
	// no-op
}

static void pointer_handle_frame(void *data,
		struct wl_pointer *wl_pointer) {
	// no-op
}

static void pointer_handle_axis_source(void *data,
		struct wl_pointer *wl_pointer, uint32_t axis_source) {
	// no-op
}

static void pointer_handle_axis_stop(void *data,
		struct wl_pointer *wl_pointer, uint32_t time,
		uint32_t axis) {
	// no-op
}

static void pointer_handle_axis_discrete(void *data,
		struct wl_pointer *wl_pointer, uint32_t axis,
		int32_t discrete) {
	// no-op
}

static void pointer_handle_axis_value120(void *data,
		struct wl_pointer *wl_pointer, uint32_t axis,
		int32_t value120) {
	// no-op
}

static const struct wl_pointer_listener pointer_listener = {
	.enter = pointer_handle_enter,
	.leave = pointer_handle_leave,
	.motion = pointer_handle_motion,
	.button = pointer_handle_button,
	.axis = pointer_handle_axis,
	.frame = pointer_handle_frame,
	.axis_source = pointer_handle_axis_source,
	.axis_stop = pointer_handle_axis_stop,
	.axis_discrete = pointer_handle_axis_discrete,
	.axis_value120 = pointer_handle_axis_value120,
};

static void wp_fractional_scale_v1_handle_scale_factor(void *data,
		struct wp_fractional_scale_v1 *wp_fractional_scale_v1,
		uint32_t scale_8_24) {
	struct surface *surface = data;
	surface->server_factor = (double)scale_8_24 / (1 << 24);
	// Redraw the surface ASAP
	surface->ctx->draw(surface);
}

static const struct wp_fractional_scale_v1_listener wp_fractional_scale_v1_listener = {
	.scale_factor = wp_fractional_scale_v1_handle_scale_factor,
};

static void surface_init(struct ctx *ctx, struct surface *surface) {
	surface->ctx = ctx;
	surface->wl_surface = wl_compositor_create_surface(ctx->wl_compositor);
	assert(surface->wl_surface != NULL);
	wl_surface_set_user_data(surface->wl_surface, surface);
	surface->wp_viewport = wp_viewporter_get_viewport(ctx->wp_viewporter,
		surface->wl_surface);
	assert(surface->wp_viewport != NULL);
	surface->scale = wp_fractional_scale_manager_v1_get_fractional_scale(
		ctx->wp_fractional_scale_manager_v1, surface->wl_surface);
	assert(surface->scale != NULL);
	wp_fractional_scale_v1_add_listener(surface->scale,
		&wp_fractional_scale_v1_listener, surface);
	surface->server_factor = 1.0;
}

void toplevel_init(struct ctx *ctx, struct toplevel *toplevel) {
	surface_init(ctx, &toplevel->base);

	toplevel->xdg_surface = xdg_wm_base_get_xdg_surface(
		ctx->xdg_wm_base, toplevel->base.wl_surface);
	assert(toplevel->xdg_surface != NULL);
	toplevel->xdg_toplevel =
		xdg_surface_get_toplevel(toplevel->xdg_surface);
	assert(toplevel->xdg_toplevel != NULL);

	xdg_surface_add_listener(toplevel->xdg_surface,
		&xdg_surface_listener, toplevel);
	xdg_toplevel_add_listener(toplevel->xdg_toplevel,
		&xdg_toplevel_listener, toplevel);

	wl_surface_commit(toplevel->base.wl_surface);
}

void subsurface_init(struct ctx *ctx, struct subsurface *subsurface,
		struct surface *parent) {
	surface_init(ctx, &subsurface->base);

	subsurface->wl_subsurface = wl_subcompositor_get_subsurface(
		ctx->wl_subcompositor, subsurface->base.wl_surface,
		parent->wl_surface);
	assert(subsurface->wl_subsurface != NULL);

	wl_subsurface_set_desync(subsurface->wl_subsurface);
}

void ctx_init(struct ctx *ctx) {
	ctx->running = true;

	ctx->wl_display = wl_display_connect(NULL);
	assert(ctx->wl_display != NULL);

	struct wl_registry *registry = wl_display_get_registry(ctx->wl_display);
	wl_registry_add_listener(registry, &registry_listener, ctx);
	wl_display_roundtrip(ctx->wl_display);

	assert(ctx->wl_shm != NULL);
	assert(ctx->wl_compositor != NULL);
	assert(ctx->wl_subcompositor != NULL);
	assert(ctx->xdg_wm_base != NULL);
	assert(ctx->wp_viewporter != NULL);
	assert(ctx->wp_fractional_scale_manager_v1 != NULL);

	assert(ctx->wl_seat != NULL);
	ctx->wl_pointer = wl_seat_get_pointer(ctx->wl_seat);
	assert(ctx->wl_pointer != NULL);
	wl_pointer_add_listener(ctx->wl_pointer, &pointer_listener, ctx);
}
