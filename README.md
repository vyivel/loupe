# Loupe

Loupe is an example Wayland compositor which implements fractional scaling via
[changing coordinate spaces](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/149).

## Building

```
meson build/
meson compile -C build/
```

## Server

```
build/src/loupe [shell command]
```

Controls:

- Alt + LMB: move focused window
- Alt + RMB: resize focused window
- Alt + h: decrease focused output's scale by 0.05
- Alt + l: increase focused output's scale by 0.05
- Alt + j: focus next output
- Alt + k: focus previous output
- Alt + Escape: exit Loupe

Loupe will try to preserve windows' logical size when moving between outputs and changing output scale.

Loupe sets the server scale factor equal to the primary output's scale, where primary output is defined as
the output with the largest area of the window.

## Clients

```
build/clients/<name>
```

- `lines`: displays vertical and horizontal lines with the width of 1 pixel.
- `subsurfaces`: displays subsurfaces with different scale factors.
  - Toplevel: the factor is equal to the client factor.
  - Red subsurface: the factor is 2.0.
  - Blue subsurface: the factor is equal to the client factor divided by 2.0.
  - Each subsurface highlights the current position of the surface with
    a green rectangle.
  - The toplevel surface has black rectangles which are expected to be fully covered
    by subsurfaces.
- `text`: displays text rendered via Pango.
- `integer`: the factor is equal to the client factor rounded up to an integer number.
  - The current position of the surface is highlighted with a black rectangle.
- `factor_loop`: continuously updates the factor in `[1..2]` range.
- `sizes`: displays subsurfaces with different ways of setting the size.
  - See `clients/sizes.c` for more information.
