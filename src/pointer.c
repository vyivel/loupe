#include <string.h>
#include <wlr/types/wlr_pointer.h>

#include "loupe.h"

void pointer_create(struct wlr_input_device *device) {
	wlr_cursor_attach_input_device(server.cursor, device);

	struct wlr_pointer *pointer = wlr_pointer_from_input_device(device);

	struct loupe_output *output;
	wl_list_for_each (output, &server.outputs, link) {
		if (strcmp(output->wlr_output->name, pointer->output_name) == 0) {
			wlr_cursor_map_input_to_output(server.cursor,
				device, output->wlr_output);
			break;
		}
	}
}
