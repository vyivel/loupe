#ifndef LOUPE_H
#define LOUPE_H

#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/allocator.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_fractional_scale_v1.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_shell.h>

struct loupe_server {
	struct wl_display *display;
	struct wlr_renderer *renderer;
	struct wlr_allocator *allocator;

	struct wl_list outputs; // struct loupe_output
	struct wlr_output_layout *output_layout;

	struct wlr_xdg_shell *xdg_shell;

	struct wlr_seat *seat;
	struct wlr_cursor *cursor;
	struct wlr_xcursor_manager *xcursor_manager;

	struct wl_list keyboards; // struct loupe_keyboard

	struct loupe_output *active_output;

	struct wl_list windows; // struct loupe_window

	struct loupe_window *active_window;

	enum {
		LOUPE_CURSOR_NORMAL = 0,
		LOUPE_CURSOR_MOVE,
		LOUPE_CURSOR_RESIZE,
	} cursor_mode;
	struct wlr_fbox cursor_grab_geom;
	struct {
		double x, y;
	} cursor_grab;
	struct loupe_window *cursor_window;
	struct wlr_surface *cursor_surface;

	struct wl_listener new_input;
	struct wl_listener new_output;

	struct wl_listener output_layout_change;

	struct wl_listener new_xdg_surface;

	struct wl_listener cursor_motion;
	struct wl_listener cursor_motion_absolute;
	struct wl_listener cursor_button;
	struct wl_listener cursor_axis;
	struct wl_listener cursor_frame;
};

struct loupe_window {
	struct wlr_xdg_toplevel *toplevel;
	bool mapped;
	double x, y; // Logical
	struct loupe_output *output; // Primary
	struct wl_list link; // struct loupe_server.windows
	double factor;

	struct wl_listener map;
	struct wl_listener unmap;
	struct wl_listener destroy;
	struct wl_listener request_move;
	struct wl_listener request_resize;
};

struct loupe_output {
	struct wlr_output *wlr_output;
	struct wl_list link; // struct loupe_server.outputs

	struct wl_listener needs_frame;
	struct wl_listener frame;
	struct wl_listener destroy;
};

struct loupe_keyboard {
	struct wlr_keyboard *wlr_keyboard;
	struct wl_list link; // struct loupe_server.keyboards

	struct wl_listener key;
	struct wl_listener modifiers;
	struct wl_listener destroy;
};

extern struct loupe_server server;

void output_create(struct wlr_output *wlr_output);
void output_all_schedule_frame();

void render_output(struct loupe_output *output);

void cursor_create();
void cursor_start_move(struct loupe_window *window);
void cursor_start_resize(struct loupe_window *window);

void keyboard_create(struct wlr_input_device *device);
void pointer_create(struct wlr_input_device *device);

void window_create(struct wlr_xdg_toplevel *toplevel);
// In logical space
void window_warp(struct loupe_window *window, double x, double y);
// In logical space
void window_resize(struct loupe_window *window, double width, double height);
void window_focus(struct loupe_window *window);
void window_update_output(struct loupe_window *window);

void server_init(char *cmd);
void server_start();

#endif
