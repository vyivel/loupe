#include <wlr/types/wlr_subcompositor.h>
#include <linux/input-event-codes.h>

#include "loupe.h"

static void reset_image() {
	wlr_xcursor_manager_set_cursor_image(server.xcursor_manager,
		"left_ptr", server.cursor);
}

static bool check_window(struct loupe_window *window, double *sx, double *sy) {
	double x = server.cursor->x - window->x;
	double y = server.cursor->y - window->y;
	struct wlr_surface *surface =
		wlr_xdg_surface_surface_at(window->toplevel->base, x, y, sx, sy);
	if (surface != NULL) {
		server.cursor_surface = surface;
		return true;
	}
	return false;
}

static void find_window(double *sx, double *sy) {
	struct loupe_window *window;
	wl_list_for_each_reverse (window, &server.windows, link) {
		if (!window->mapped) {
			continue;
		}
		if (check_window(window, sx, sy)) {
			server.cursor_window = window;
			break;
		}
	}
}

static void move(uint32_t time) {
	if (server.cursor_mode == LOUPE_CURSOR_MOVE) {
		window_warp(server.cursor_window,
			server.cursor_grab_geom.x + server.cursor->x -
				server.cursor_grab.x,
			server.cursor_grab_geom.y + server.cursor->y -
				server.cursor_grab.y);
		return;
	} else if (server.cursor_mode == LOUPE_CURSOR_RESIZE) {
		window_resize(server.cursor_window,
			server.cursor_grab_geom.width + server.cursor->x -
				server.cursor_grab.x,
			server.cursor_grab_geom.height + server.cursor->y -
				server.cursor_grab.y);
		return;
	}

	server.cursor_window = NULL;
	struct wlr_surface *old = server.cursor_surface;
	server.cursor_surface = NULL;

	double sx, sy;
	find_window(&sx, &sy);

	if (server.cursor_window == NULL) {
		reset_image();
	}
	if (server.cursor_surface != NULL) {
		wlr_seat_pointer_notify_enter(server.seat,
			server.cursor_surface, sx, sy);
		if (old == server.cursor_surface) {
			wlr_seat_pointer_notify_motion(server.seat, time, sx, sy);
		}
	} else {
		wlr_seat_pointer_clear_focus(server.seat);
	}
}

static void handle_motion(struct wl_listener *listener, void *data) {
	struct wlr_pointer_motion_event *event = data;
	wlr_cursor_move(server.cursor, &event->pointer->base,
		event->delta_x, event->delta_y);
	move(event->time_msec);
}

static void handle_motion_absolute(struct wl_listener *listener, void *data) {
	struct wlr_pointer_motion_absolute_event *event = data;
	wlr_cursor_warp_absolute(server.cursor, &event->pointer->base,
		event->x, event->y);
	move(event->time_msec);
}

static void handle_button(struct wl_listener *listener, void *data) {
	struct wlr_pointer_button_event *event = data;

	double sx, sy;
	find_window(&sx, &sy);

	if (event->state == WLR_BUTTON_PRESSED) {
		struct wlr_keyboard *wlr_keyboard = wlr_seat_get_keyboard(server.seat);
		if (server.cursor_window != NULL && wlr_keyboard != NULL) {
			uint32_t modifiers = wlr_keyboard_get_modifiers(wlr_keyboard);
			if ((modifiers & WLR_MODIFIER_ALT) != 0) {
				if (event->button == BTN_LEFT) {
					cursor_start_move(server.cursor_window);
				} else if (event->button == BTN_RIGHT) {
					cursor_start_resize(server.cursor_window);
				}
				return;
			}
		}
		window_focus(server.cursor_window);
	} else {
		if (server.cursor_mode != LOUPE_CURSOR_NORMAL) {
			server.cursor_mode = LOUPE_CURSOR_NORMAL;
			return;
		}
	}
	wlr_seat_pointer_notify_button(server.seat,
		event->time_msec, event->button, event->state);
}

static void handle_axis(struct wl_listener *listener, void *data) {
	struct wlr_pointer_axis_event *event = data;

	double sx, sy;
	find_window(&sx, &sy);

	wlr_seat_pointer_notify_axis(server.seat,
		event->time_msec, event->orientation, event->delta,
		event->delta_discrete, event->source);
}

static void handle_frame(struct wl_listener *listener, void *data) {
	wlr_seat_pointer_notify_frame(server.seat);
}

void cursor_start_move(struct loupe_window *window) {
	server.cursor_mode = LOUPE_CURSOR_MOVE;
	server.cursor_grab.x = server.cursor->x;
	server.cursor_grab.y = server.cursor->y;
	server.cursor_grab_geom.x = window->x;
	server.cursor_grab_geom.y = window->y;
}

void cursor_start_resize(struct loupe_window *window) {
	server.cursor_mode = LOUPE_CURSOR_RESIZE;
	server.cursor_grab.x = server.cursor->x;
	server.cursor_grab.y = server.cursor->y;
	struct wlr_fbox box;
	wlr_xdg_surface_get_geometry(window->toplevel->base, &box);
	server.cursor_grab_geom.width = box.width;
	server.cursor_grab_geom.height = box.height;
}

void cursor_create() {
	server.cursor = wlr_cursor_create();
	wlr_cursor_attach_output_layout(server.cursor, server.output_layout);

	server.cursor_motion.notify = handle_motion;
	wl_signal_add(&server.cursor->events.motion, &server.cursor_motion);
	server.cursor_motion_absolute.notify = handle_motion_absolute;
	wl_signal_add(&server.cursor->events.motion_absolute,
		&server.cursor_motion_absolute);
	server.cursor_button.notify = handle_button;
	wl_signal_add(&server.cursor->events.button, &server.cursor_button);
	server.cursor_axis.notify = handle_axis;
	wl_signal_add(&server.cursor->events.axis, &server.cursor_axis);
	server.cursor_frame.notify = handle_frame;
	wl_signal_add(&server.cursor->events.frame, &server.cursor_frame);
};
