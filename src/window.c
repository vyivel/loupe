#include <stdlib.h>
#include <math.h>

#include "loupe.h"

static void handle_map(struct wl_listener *listener, void *data) {
	struct loupe_window *window =
		wl_container_of(listener, window, map);
	wl_signal_add(&window->toplevel->events.request_move, &window->request_move);
	wl_signal_add(&window->toplevel->events.request_resize, &window->request_resize);
	window->mapped = true;
	window_update_output(window);
	output_all_schedule_frame();
}

static void handle_unmap(struct wl_listener *listener, void *data) {
	struct loupe_window *window =
		wl_container_of(listener, window, unmap);
	wl_list_remove(&window->request_move.link);
	wl_list_remove(&window->request_resize.link);
	window->mapped = false;
	if (window == server.active_window) {
		server.active_window = NULL;
	}
	if (window == server.cursor_window) {
		server.cursor_mode = LOUPE_CURSOR_NORMAL;
		server.cursor_window = NULL;
	}
	output_all_schedule_frame();
}

static void handle_destroy(struct wl_listener *listener, void *data) {
	struct loupe_window *window =
		wl_container_of(listener, window, destroy);
	wl_list_remove(&window->map.link);
	wl_list_remove(&window->unmap.link);
	wl_list_remove(&window->destroy.link);
	wl_list_remove(&window->link);
	free(window);
}

static void handle_request_move(struct wl_listener *listener, void *data) {
	struct loupe_window *window =
		wl_container_of(listener, window, request_move);
	cursor_start_move(window);
}

static void handle_request_resize(struct wl_listener *listener, void *data) {
	struct loupe_window *window =
		wl_container_of(listener, window, request_resize);
	cursor_start_resize(window);
}

static void snap(struct loupe_window *window) {
	if (window->output != NULL) {
		// Snap to primary output's pixel grid
		double factor = window->output->wlr_output->scale;
		window->x = round(window->x * factor) / factor;
		window->y = round(window->y * factor) / factor;
	}
}

static void send_scale_factor_iter(struct wlr_surface *surface,
		double sx, double sy, void *data) {
	struct loupe_window *window = data;
	wlr_fractional_scale_v1_send_scale_factor(surface, window->factor);
}

void window_update_output(struct loupe_window *window) {
	struct wlr_fbox box;
	wlr_xdg_surface_get_geometry(window->toplevel->base, &box);
	box.x += window->x;
	box.y += window->y;

	window->output = NULL;
	double biggest = 0.0;

	struct loupe_output *output;
	wl_list_for_each (output, &server.outputs, link) {
		struct wlr_box output_ibox;
		wlr_output_layout_get_box(server.output_layout,
			output->wlr_output, &output_ibox);
		struct wlr_fbox output_box = {
			.x = output_ibox.x,
			.y = output_ibox.y,
			.width = output_ibox.width,
			.height = output_ibox.height,
		};
		struct wlr_fbox intersection;
		if (!wlr_fbox_intersection(&intersection, &box, &output_box)) {
			continue;
		}
		double area = intersection.width * intersection.height;
		if (area > biggest) {
			biggest = area;
			window->output = output;
		}
	}

	if (window->output == NULL) {
		return;
	}
	if (window->factor != window->output->wlr_output->scale) {
		window->factor = window->output->wlr_output->scale;
		snap(window);
		wlr_xdg_surface_for_each_surface(window->toplevel->base,
			send_scale_factor_iter, window);
	}
}

void window_warp(struct loupe_window *window, double x, double y) {
	window->x = x;
	window->y = y;
	snap(window);
	window_update_output(window);
}

void window_resize(struct loupe_window *window, double width, double height) {
	wlr_xdg_toplevel_set_size(window->toplevel, width, height);
}

void window_focus(struct loupe_window *window) {
	if (server.active_window == window) {
		return;
	}
	if (server.active_window != NULL) {
		wlr_xdg_toplevel_set_activated(server.active_window->toplevel, false);
	}
	if (window != NULL) {
		wlr_xdg_toplevel_set_activated(window->toplevel, true);
		struct wlr_keyboard *keyboard = wlr_seat_get_keyboard(server.seat);
		if (keyboard != NULL) {
			wlr_seat_keyboard_notify_enter(server.seat,
				window->toplevel->base->surface,
				keyboard->keycodes, keyboard->num_keycodes,
				&keyboard->modifiers);
		}
		wl_list_remove(&window->link);
		wl_list_insert(server.windows.prev, &window->link);
	}
	server.active_window = window;
	output_all_schedule_frame();
}

void window_create(struct wlr_xdg_toplevel *toplevel) {
	struct loupe_window *window = calloc(1, sizeof(*window));
	if (window == NULL) {
		fprintf(stderr, "Failed to allocate a window\n");
		return;
	}
	window->toplevel = toplevel;

	window->map.notify = handle_map;
	wl_signal_add(&toplevel->base->events.map, &window->map);
	window->unmap.notify = handle_unmap;
	wl_signal_add(&toplevel->base->events.unmap, &window->unmap);
	window->destroy.notify = handle_destroy;
	wl_signal_add(&toplevel->base->events.destroy, &window->destroy);
	window->request_move.notify = handle_request_move;
	window->request_resize.notify = handle_request_resize;

	window->factor = 1.0;

	wl_list_insert(server.windows.prev, &window->link);
}
