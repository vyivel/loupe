#include <stdio.h>

#include "loupe.h"

int main(int argc, char **argv) {
	if (argc != 1 && argc != 2) {
		fprintf(stderr, "usage: %s [shell command]\n", argv[0]);
		return 1;
	}
	char *cmd = NULL;
	if (argc == 2) {
		cmd = argv[1];
	}
	server_init(cmd);
	server_start();
	return 0;
}
