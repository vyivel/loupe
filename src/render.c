#include <math.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_fractional_scale_v1.h>
#include <wlr/types/wlr_subcompositor.h>
#include <wlr/types/wlr_matrix.h>

#include "loupe.h"

struct render_data {
	double ox, oy;
	struct loupe_output *output;
	struct timespec *when;
};

// x, y are in output-local space
static void render_surface(struct wlr_surface *surface, double x, double y,
		void *data) {
	struct render_data *render_data = data;
	double output_factor = render_data->output->wlr_output->scale;

	struct wlr_texture *texture = wlr_surface_get_texture(surface);
	if (texture != NULL) {
		struct wlr_fbox src_box;
		wlr_surface_get_buffer_source_box(surface, &src_box);
		float mat[9];
		x += render_data->ox;
		y += render_data->oy;
		struct wlr_box box = {
			.x = x * output_factor,
			.y = y * output_factor,
			.width = surface->current.width * output_factor,
			.height = surface->current.height * output_factor,
		};
		wlr_matrix_project_box(mat, &box,
			wlr_output_transform_invert(surface->current.transform),
			0.0f, render_data->output->wlr_output->transform_matrix);
		wlr_render_subtexture_with_matrix(server.renderer,
			texture, &src_box, mat, 1);
		wlr_surface_send_frame_done(surface, render_data->when);
	}
}

static void render_window(struct loupe_output *output,
		struct loupe_window *window, struct timespec *when) {
	double ox = window->x, oy = window->y;
	wlr_output_layout_output_coords(server.output_layout,
		output->wlr_output, &ox, &oy);
	struct render_data render_data = {
		.ox = ox,
		.oy = oy,
		.output = output,
		.when = when,
	};
	wlr_xdg_surface_for_each_surface(window->toplevel->base,
		render_surface, &render_data);
}

void render_output(struct loupe_output *output) {
	struct wlr_output *wlr_output = output->wlr_output;
	struct wlr_renderer *renderer = server.renderer;

	wlr_output_attach_render(wlr_output, NULL);
	wlr_renderer_begin(renderer, wlr_output->width, wlr_output->height);

	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

	float color[4] = {0.1f, 0.1f, 0.1f, 1.0f};
	if (output == server.active_output) {
		color[2] = 0.15f;
	}
	wlr_renderer_clear(renderer, color);

	struct loupe_window *window;
	wl_list_for_each (window, &server.windows, link) {
		if (!window->mapped) {
			continue;
		}
		render_window(output, window, &now);
	}

	wlr_renderer_end(renderer);

	wlr_output_render_software_cursors(wlr_output, NULL);

	wlr_output_commit(wlr_output);
}
