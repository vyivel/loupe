#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_subcompositor.h>
#include <wlr/types/wlr_viewporter.h>

#include "loupe.h"

struct loupe_server server = {0};

static void handle_new_input(struct wl_listener *listener, void *data) {
	struct wlr_input_device *device = data;
	switch (device->type) {
	case WLR_INPUT_DEVICE_KEYBOARD:
		keyboard_create(device);
		break;
	case WLR_INPUT_DEVICE_POINTER:
		pointer_create(device);
		break;
	default:
		break;
	}
}

static void handle_new_output(struct wl_listener *listener, void *data) {
	struct wlr_output *wlr_output = data;
	if (wlr_output->non_desktop) {
		return;
	}
	output_create(wlr_output);
}

static void handle_new_xdg_surface(struct wl_listener *listener, void *data) {
	struct wlr_xdg_surface *surface = data;
	if (surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL) {
		return;
	}
	window_create(surface->toplevel);
}

static void handle_output_layout_change(struct wl_listener *listener, void *data) {
	struct loupe_window *window;
	wl_list_for_each (window, &server.windows, link) {
		window_update_output(window);
	}
	output_all_schedule_frame();
}

void server_init(char *cmd) {
	server.display = wl_display_create();

	struct wlr_backend *backend = wlr_backend_autocreate(server.display);
	if (backend == NULL) {
		fprintf(stderr, "Failed to create a backend\n");
		exit(1);
	}

	const char *socket = wl_display_add_socket_auto(server.display);
	setenv("WAYLAND_DISPLAY", socket, true);

	if (cmd != NULL) {
		if (fork() == 0) {
			execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);
		}
	}

	server.renderer = wlr_renderer_autocreate(backend);
	server.allocator = wlr_allocator_autocreate(backend, server.renderer);

	wlr_renderer_init_wl_display(server.renderer, server.display);

	wl_list_init(&server.outputs);
	server.output_layout = wlr_output_layout_create();
	server.output_layout_change.notify = handle_output_layout_change;
	wl_signal_add(&server.output_layout->events.change, &server.output_layout_change);

	wl_list_init(&server.keyboards);

	server.new_input.notify = handle_new_input;
	wl_signal_add(&backend->events.new_input, &server.new_input);

	server.new_output.notify = handle_new_output;
	wl_signal_add(&backend->events.new_output, &server.new_output);

	server.seat = wlr_seat_create(server.display, "seat0");
	wlr_seat_set_capabilities(server.seat, WL_SEAT_CAPABILITY_KEYBOARD |
		WL_SEAT_CAPABILITY_POINTER);

	server.xcursor_manager = wlr_xcursor_manager_create(NULL, 24);
	wlr_xcursor_manager_load(server.xcursor_manager, 1);

	cursor_create();

	wlr_compositor_create(server.display, server.renderer);
	wlr_subcompositor_create(server.display);
	wlr_viewporter_create(server.display);
	wlr_data_device_manager_create(server.display);

	wlr_fractional_scale_manager_v1_create(server.display);

	wl_list_init(&server.windows);

	server.xdg_shell = wlr_xdg_shell_create(server.display, 5);
	server.new_xdg_surface.notify = handle_new_xdg_surface;
	wl_signal_add(&server.xdg_shell->events.new_surface,
		&server.new_xdg_surface);

	if (!wlr_backend_start(backend)) {
		fprintf(stderr, "Failed to start the backend\n");
		exit(1);
	}
}

void server_start() {
	wl_display_run(server.display);
}
