#include <stdlib.h>
#include <wlr/types/wlr_keyboard.h>
#include <xkbcommon/xkbcommon.h>

#include "loupe.h"

static void handle_destroy(struct wl_listener *listener, void *data) {
	struct loupe_keyboard *keyboard =
		wl_container_of(listener, keyboard, destroy);
	wl_list_remove(&keyboard->destroy.link);
	wl_list_remove(&keyboard->key.link);
	wl_list_remove(&keyboard->modifiers.link);
	wl_list_remove(&keyboard->link);
	free(keyboard);
}

static void handle_modifiers(struct wl_listener *listener, void *data) {
	struct loupe_keyboard *keyboard =
		wl_container_of(listener, keyboard, modifiers);
	wlr_seat_set_keyboard(server.seat, keyboard->wlr_keyboard);
	wlr_seat_keyboard_notify_modifiers(server.seat,
		&keyboard->wlr_keyboard->modifiers);
}

static bool process_binding(uint32_t sym) {
	switch (sym) {
	case XKB_KEY_Escape:
		wl_display_terminate(server.display);
		break;
	case XKB_KEY_j:
		// Focus next output
		if (server.active_output != NULL) {
			struct wl_list *link = server.active_output->link.next;
			if (link == &server.outputs) {
				link = server.outputs.next;
			}
			server.active_output = wl_container_of(link,
				server.active_output, link);
			output_all_schedule_frame();
		}
		break;
	case XKB_KEY_k:
		// Focus previous output
		if (server.active_output != NULL) {
			struct wl_list *link = server.active_output->link.prev;
			if (link == &server.outputs) {
				link = server.outputs.prev;
			}
			server.active_output = wl_container_of(link,
				server.active_output, link);
			output_all_schedule_frame();
		}
		break;
	case XKB_KEY_l:
		if (server.active_output != NULL) {
			struct wlr_output *wlr_output = server.active_output->wlr_output;
			wlr_output_set_scale(wlr_output, wlr_output->scale + 0.05f);
			wlr_output_commit(wlr_output);
			printf("[+] %s: %fx\n", wlr_output->name, wlr_output->scale);
		}
		break;
	case XKB_KEY_h:
		if (server.active_output != NULL) {
			struct wlr_output *wlr_output = server.active_output->wlr_output;
			wlr_output_set_scale(wlr_output, wlr_output->scale - 0.05f);
			wlr_output_commit(wlr_output);
			printf("[+] %s: %fx\n", wlr_output->name, wlr_output->scale);
		}
		break;
	default:
		return false;
	}
	return true;
}

static void handle_key(struct wl_listener *listener, void *data) {
	struct loupe_keyboard *keyboard =
		wl_container_of(listener, keyboard, key);
	struct wlr_keyboard_key_event *event = data;
	uint32_t modifiers = wlr_keyboard_get_modifiers(keyboard->wlr_keyboard);
	if ((modifiers & WLR_MODIFIER_ALT) != 0 &&
			 event->state == WL_KEYBOARD_KEY_STATE_PRESSED) {
		uint32_t keycode = event->keycode + 8;
		const xkb_keysym_t *syms;
		int nsyms = xkb_state_key_get_syms(
			keyboard->wlr_keyboard->xkb_state, keycode, &syms);
		bool handled = false;
		for (int i = 0; i < nsyms; i++) {
			handled |= process_binding(syms[i]);
		}
		if (handled) {
			return;
		}
	}
	wlr_seat_set_keyboard(server.seat, keyboard->wlr_keyboard);
	wlr_seat_keyboard_notify_key(server.seat, event->time_msec,
		event->keycode, event->state);
}

void keyboard_create(struct wlr_input_device *device) {
	struct loupe_keyboard *keyboard = calloc(1, sizeof(*keyboard));
	if (keyboard == NULL) {
		fprintf(stderr, "Failed to allocate a keyboard\n");
		return;
	}
	keyboard->wlr_keyboard = wlr_keyboard_from_input_device(device);

	struct xkb_context *context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	struct xkb_keymap *keymap = xkb_keymap_new_from_names(context, NULL,
		XKB_KEYMAP_COMPILE_NO_FLAGS);

	wlr_keyboard_set_keymap(keyboard->wlr_keyboard, keymap);
	xkb_keymap_unref(keymap);
	xkb_context_unref(context);
	wlr_keyboard_set_repeat_info(keyboard->wlr_keyboard, 25, 600);

	keyboard->key.notify = handle_key;
	wl_signal_add(&keyboard->wlr_keyboard->events.key, &keyboard->key);
	keyboard->modifiers.notify = handle_modifiers;
	wl_signal_add(&keyboard->wlr_keyboard->events.modifiers, &keyboard->modifiers);
	keyboard->destroy.notify = handle_destroy;
	wl_signal_add(&device->events.destroy, &keyboard->destroy);

	wl_list_insert(server.keyboards.prev, &keyboard->link);
}
