#include <stdio.h>
#include <stdlib.h>

#include "loupe.h"

static void handle_destroy(struct wl_listener *listener, void *data) {
	struct loupe_output *output =
		wl_container_of(listener, output, destroy);

	if (server.active_output == output) {
		if (!wl_list_empty(&server.outputs)) {
			server.active_output = wl_container_of(
				server.outputs.prev, output, link);
		} else {
			server.active_output = NULL;
		}
	}

	wl_list_remove(&output->destroy.link);
	wl_list_remove(&output->needs_frame.link);
	wl_list_remove(&output->frame.link);
	wl_list_remove(&output->link);
	free(output);
}

static void handle_needs_frame(struct wl_listener *listener, void *data) {
	struct loupe_output *output =
		wl_container_of(listener, output, needs_frame);
	wlr_output_schedule_frame(output->wlr_output);
}

static void handle_frame(struct wl_listener *listener, void *data) {
	struct loupe_output *output =
		wl_container_of(listener, output, frame);
	render_output(output);
}

void output_all_schedule_frame() {
	struct loupe_output *output;
	wl_list_for_each (output, &server.outputs, link) {
		wlr_output_schedule_frame(output->wlr_output);
	}
}

void output_create(struct wlr_output *wlr_output) {
	struct loupe_output *output = calloc(1, sizeof(*output));
	if (output == NULL) {
		fprintf(stderr, "Failed to allocate an output\n");
		return;
	}
	output->wlr_output = wlr_output;

	wlr_output_init_render(wlr_output, server.allocator, server.renderer);

	output->destroy.notify = handle_destroy;
	wl_signal_add(&wlr_output->events.destroy, &output->destroy);

	output->needs_frame.notify = handle_needs_frame;
	wl_signal_add(&wlr_output->events.needs_frame, &output->needs_frame);

	output->frame.notify = handle_frame;
	wl_signal_add(&wlr_output->events.frame, &output->frame);

	wlr_output_layout_add_auto(server.output_layout, wlr_output);
	wl_list_insert(server.outputs.prev, &output->link);

	wlr_output_enable(wlr_output, true);

	struct wlr_output_mode *mode = wlr_output_preferred_mode(wlr_output);
	if (mode != NULL) {
		wlr_output_set_mode(wlr_output, mode);
	}

	if (!wlr_output_commit(wlr_output)) {
		fprintf(stderr, "Failed to commit an output\n");
	}

	if (server.active_output == NULL) {
		server.active_output = output;
	}
}
